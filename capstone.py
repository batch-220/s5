# Specifications
# 1. Create a Person class that is an abstract class that has the following methods
#   a. getFullName method
#   b. addRequest method
#   c. checkRequest method
#   d. addUser method

# 2. Create an Employee class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
#       Abstract methods
#       For the checkRequest and addUser methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

# 3. Create a TeamLead class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department, members
#   b. Methods
#       Abstract methods
#       For the addRequest and addUser methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addMember() - adds an employee to the members list
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

# 4. Create an Admin class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
#       Abstract methods
#       For the checkRequest and addRequest methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addUser() - outputs "New user added"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

# 5. Create a Request class that has the following properties and methods
#   a. properties
#       name, requester, dateRequested, status
#   b. Methods
#       updateRequest
#       closeRequest
#       cancelRequest
#       Note: All methods just return Strings of simple text
#           Ex. Request < name > has been updated/closed/cancelled


# Solution
from abc import ABC, abstractclassmethod


class Person_Methods(ABC):
    @abstractclassmethod
    def get_full_name(self):
        pass

    @abstractclassmethod
    def login(self):
        pass

    @abstractclassmethod
    def logout(self):
        pass


class Person(Person_Methods):
    def __init__(self, first_name, last_name, email, department):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    # Getters
    def get_first_name(self):
        return self._first_name

    def get_last_name(self):
        return self._last_name

    def get_email(self):
        return self._email

    def get_department(self):
        return self._department

    # Setters
    def set_first_name(self, first_name):
        self._first_name = first_name

    def set_last_name(self, last_name):
        self._last_name = last_name

    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._department = department

    # Re-implimentation of login() and logout()
    def get_full_name(self):
        return f"{self._first_name} {self._last_name}"

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"




class Employee(Person):
    def add_request(self):
        return "Request has been added"


class Team_Lead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)
        self._members = []

    # Getter and Setter for the new property
    def get_members(self):
        return self._members

    def set_members(self, members):
        self._members = members

    # Method
    def add_member(self, member):
        self._members.append(member)

    def check_request(self):
        return "Request has been checked"


class Admin(Person):
    def add_user(self):
        return "User has been added"



class Request():
    def __init__(self, name, requester, date_requested):
        self._name = name
        self._requester = requester
        self._date_requested = date_requested
        self.status = "open"

    # Getters
    def get_name(self):
        return self._name

    def get_requester(self):
        return self._requester

    def get_date_requested(self):
        return self._date_requested

    def get_status(self):
        return self._status

    # Setters
    def set_name(self, name):
        self._name = name

    def set_requester(self, requester):
        self._requester = requester

    def set_date_request(self, date_requested):
        self._date_requested = date_requested

    def set_status(self, status):
        self._status = status

    # Methods
    def update_request(self):
        return f"Request {self._name} has been updated"

    def close_request(self):
        return f"Request {self._name} has been closed"

    def cancel_request(self):
        return f"Request {self._name} has been cancelled"
    


# Test Cases
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")

admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
team_lead1 = Team_Lead("Michael", "Specter", "smichael@mail.com", "Sales")

req1 = Request("New hire orientation", team_lead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.get_full_name() == "John Doe", "Full name should be John Doe"
assert admin1.get_full_name() == "Monika Justin", "Full name should be Monika Justin"
assert team_lead1.get_full_name() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.add_request() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

team_lead1.add_member(employee3)
team_lead1.add_member(employee4)

for indiv_emp in team_lead1.get_members():
    print(indiv_emp.get_full_name())

assert admin1.add_user() == "User has been added"

req2.set_status("closed")
print(req2.close_request())